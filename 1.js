
var SeleccionFutbol=function(padre){// funcion prototipo

	this.id=null;
	this.nombre=null;
	this.apellidos=null;
	this.edad=null;

	this.getId=function(){
		return this.id;
	};
	this.setId=function(v){
		this.id=v || 0;
	};
	this.getNombre=function(){
		return this.nombre;
	};
	this.setNombre=function(v){
		this.nombre=v || "";
	};
	this.getApellidos=function(){
		return this.apellidos;
	};
	this.setApellidos=function(v){
		this.apellidos=v || "";
	};
	this.getEdad=function(){
		return this.edad;
	};
	this.setEdad=function(v){
		this.edad=v || 0;
	};

	this.concentrarse=function(){
		return "Me estoy concentrando";
	};
	this.viajar=function(){
		return "volando voy, volando vengo";
	};

	this.seleccionFutbol=function(p){
		this.setId(p.id);
		this.setNombre(p.nombre);
		this.setApellidos(p.apellidos);
		this.setEdad(p.edad);
		
	};
	this.seleccionFutbol(padre);

};

var Futbolista=function(hijo){

	this.dorsal=null;
	this.demarcacion=null;

	this.getDorsal=function(){
		return this.dorsal;
	}
	this.setDorsal=function(v){
		this.dorsal=v|| "";
	};
	this.getDemarcacion=function(){
		return this.demarcacion;
	}
	this.setDemarcacion=function(v){
		this.demarcacion=v ||0;
	};

	this.jugarPartido=function(){
		return "Jugando";
	};
	this.entrenar=function(){
		return "Entrenar";
	};

	this.futbolista=function(h){
		SeleccionFutbol.call(this,h);
		this.setDorsal(h.dorsal);
		this.setDemarcacion(h.demarcacion);
		
	};

	this.futbolista(hijo);
	 // if(typeof(hijo)=="undefined"){
	 // 	this.futbolista({});
	 // }else{
	 	
	 // } //<--- forma de sobrecargar un metodo

};

var Entrenador=function(hijo){

	this.idFederacion=0;
	
	this.getIdFederacion=function(){
		return this.idFederacion;
	};
	this.setIdFederacion=function(v){
		this.idFederacion=v || 0;
	};

	this.dirigirPartido=function(){
		return "dirigiendo";
	};
	this.dirigirEntrenamiento=function(){
		return "Entrenando peña";
	};

	this.entrenador=function(h){		
		SeleccionFutbol.call(this,h);
		this.setIdFederacion(h.idFederacion);
	};
	this.entrenador(hijo);

};

var Masajista=function(hijo){

	this.titulacion=null;
	this.aniosExperiencia=null;

	this.getTitulacion=function(){
		return this.titulacion;
	};
	this.setTitulacion=function(v){
		 this.titulacion=v || "";
	};
	this.getAniosExperiencia=function(){
		return this.aniosExperiencia;
	};
	this.setAniosExperiencia=function(v){
		 this.aniosExperiencia=v || 0;
	};
	

	this.darMasaje=function(){
		console.log("masajeando");
	};
	
	this.masajista=function(h){
		SeleccionFutbol.call(this,h);
		this.setTitulacion(h.titulacion);
		this.setAniosExperiencia(h.aniosExperiencia);		
	};
	this.masajista(hijo);

};

var uno=new SeleccionFutbol({
	id:1,	
	nombre:"pepe",
	apellidos:"los palotes",
	edad:31
});
var dos=new Futbolista({
	id:2,	
	nombre:"cristiano",
	apellidos:"ronaldo",
	edad:33,
	dorsal:7,
	demarcacion:"delantero"
});
var tres=new Entrenador({
	id:3,	
	nombre:"jose",
	apellidos:"mourinho da sousa",
	edad:56,
	idFederacion:2450003
});
var cuatro=new Masajista({
	id:4,	
	nombre:"vicky",
	apellidos:"gonzalez",
	edad:27,
	titulacion:"standford",
	aniosExperiencia:7

});

console.log(uno);
console.log(dos);
console.log(tres);
console.log(cuatro);